import React from "react";

const Banner = () => {
  return (
    <div className="banner">
      <div className="container text-center">
        
        <h1>A warm welcome!</h1>
        <p>
          Bootstrap utility classes are used to create this jumbotron since the
          old component has been removed from the framework. Why create custom
          CSS when you can use utilities?
        </p>
        <button className="btn btn-primary w-20 h-10">Call to action</button>
      </div>
    </div>
  );
};

export default Banner;
