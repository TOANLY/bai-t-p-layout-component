import React from "react";

const Item = () => {
  return (
    <div className="item container text-center">
      <div className="row">
        <div className="col-4">
          <div className="icon">
            <i class="fa fa-desktop"></i>
          </div>
          <h3>Fresh new layout</h3>
          <p>
            With Bootstrap 5, we've created a fresh new layout for this
            template!
          </p>
        </div>
        <div className="col-4">
          <div className="icon">
            <i class="fa fa-cloud"></i>
          </div>
          <h3>Free to download</h3>
          <p>
            As always, Start Bootstrap has a powerful collectin of free
            templates.
          </p>
        </div>
        <div className="col-4">
          <div className="icon">
            <i class="fa fa-file-alt"></i>
          </div>
          <h3>Jumbotron hero header</h3>
          <p>The heroic part of this template is the jumbotron hero header!</p>
        </div>
      </div>
      <div className="row">
        <div className="col-4">
          <div className="icon">
            <i class="fab fa-bootstrap"></i>
          </div>
          <h3>Feature boxes</h3>
          <p>We've created some custom feature boxes using Bootstrap icons!</p>
        </div>
        <div className="col-4">
          <div className="icon">
            <i class="fa fa-code"></i>
          </div>
          <h3>Simple clean code</h3>
          <p>
            We keep our dependencies up to date and squash bugs as they come!
          </p>
        </div>
        <div className="col-4">
          <div className="icon">
            <i class="fa fa-check"></i>
          </div>
          <h3>A name you trust</h3>
          <p>
            Start Bootstrap has been the leader in free Bootstrap templates
            since 2013!
          </p>
        </div>
      </div>
    </div>
  );
};

export default Item;
