import React from "react";
// import "./Baitap.css";

const Header = () => {
  return (
    <div className="header">
      <nav className="navbar navbar-expand-lg navbar-dark ">
        <div className="container px-lg-5">
          <a className="navbar-brand" href="#">
            Start Bootstrap
          </a>
          <ul className="list d-flex mt-2">
            <li className="nav-item">
              <a href="#">Home</a>
            </li>
            <li className="nav-item">
              <a href="#">About</a>
            </li>
            <li className="nav-item">
              <a href="#">Contact</a>
            </li>
          </ul>
        </div>
      </nav>
    </div>
  );
};

export default Header;
