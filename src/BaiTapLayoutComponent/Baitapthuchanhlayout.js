import React from 'react'
import './Baitap.css'
import Header from './Header'
import Body from './Body'
import Footer from './Footer'

const Baitapthuchanhlayout = () => {
  return (
    <div>
        <Header/>
        <Body/>
        <Footer/>
    </div>
  )
}

export default Baitapthuchanhlayout