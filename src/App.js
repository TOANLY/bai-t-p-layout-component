import logo from './logo.svg';
import './App.css';
import Header from './BaiTapLayoutComponent/Header';
import Baitapthuchanhlayout from './BaiTapLayoutComponent/Baitapthuchanhlayout';

function App() {
  return (
    <div className="App">
      <Baitapthuchanhlayout/>
    </div>
  );
}

export default App;
